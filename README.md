# DN Gitlab Templates

This repo contains several template files that can be [included](https://docs.gitlab.com/ee/ci/yaml/#includeremote)
in your Gitlab CI/CD script files.  They provide some generic functionality
that can be reused across scripts, such as:

- Default test jobs
- Code coverage
- Publishing libraries and coverage information

See below for the kind of build jobs that are provided.  You can include them in your project like this:

```yaml
include:
  - remote: https://gitlab.com/dn13/oss/gitlab-templates/-/raw/main/<template-file>.yml
```

## Clojure

There are two files provided right now: `clojure-common.yml` and `clojure-lib.yml`.  The first
one contains these jobs:
- `.clj`: hidden base job for Clojure Leiningen stuff
- `clj:test`: runs unit tests with Cloverage applied for coverage
- `clj:show-coverage`: converts coverage report into Cobertura format, so Gitlab can [show it in MRs](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html).
- `pages`: publishes the coverage report as a static site using [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/index.html).

The Clojure lib template adds these steps
- `clj:publish`: Deploys the library with GPG signing.  Only when pushed to the default branch.

### Testing

The `clj:test` job runs the Midje tests, with coverage enabled, by executing the `lein cloverage`
command, with some additional arguments to indicate which kind of outputs are required.  You can
add extra arguments to leiningen (e.g. for profiles) by setting the `LEIN_TEST_ARGS` variable.
For example, when you want to include an additional profile:

```yaml
variables:
  LEIN_TEST_ARGS: with-profile +test
```

### Sensitive Information

In order to avoid having to add sensitive properties to the Gitlab configuration,
some properties are fetched from Vault.  These include the `NEXUS_USER` and `NEXUS_PASS`,
for dependency retrieval and publishing, and the `GITLAB_GPG_KEY` for artifact signing.

## Gradle

The `gradle-common.yml` file provides basic jobs for Gradle (Java) builds.
- `.gradle`: the hidden base job for Gradle build tasks.
- `gradle:build`: compiles the code, but does not test.
- `gradle:test`: runs unit tests and provides coverage (using Jacoco).
- `gradle:show-coverage`: converts coverage report into a Cobertura format this makes it [viewable in MRs](https://docs.gitlab.com/ee/user/project/merge_requests/test_coverage_visualization.html).
- `pages`: publishes the coverage report as [Gitlab pages](https://docs.gitlab.com/ee/user/project/pages/index.html).

You can pass additional options to Gradle (like source file encoding) by setting the
`GRADLE_OPTS` environment variable.

Other environment variables:
- `BUILD_DIR`: relative directory where build files are located.  The build script will `cd` into that directory before invoking gradle.  Defaults to `.`.
- `SRC_DIR`: the directory where sources are located, used for coverage reporting.  Defaults to `src/main/java/`.  Should be relative to the `$BUILD_DIR`.
- `EXTRA_BUILD_ARGS`: additional arguments passed to Gradle in the build step
- `EXTRA_TEST_ARGS`: additional arguments passed to Gradle in the test step

## Docker

The `docker.yml` file provides jobs for Docker image publishing.  There are two
jobs provided: `docker:develop` and `docker:release`.  The first one builds an image
for development/testing purposes (i.e. for deployment to staging environments), which
is pushed to the Gitlab registry, with a `develop` tag.  The release job builds an image
to use in (pre-)production environments, and is pushed to the registry defined by the
`REGISTRY_IMAGE` variable.  The release job is triggered by a tag, which should have
a pattern of `release/xxxx`, `hotfix/xxxx` or `v1234`, so the version tag is extracted
from the tag name.

When including this file, you need to provide a job called `build` which should
provide the artifacts that need to be included in the image.

By default is assumes the `Dockerfile` is located in the project directory, but this
can be overridden by specifying the `DOCKER_DIR` variable.

The docker images jobs are running in the `image` stage.

## Slack
The `slack-notify.yml` file provide jobs for notifying about publishing new docker images/tags.

You can use this remotely like this:
```yaml
include: https://gitlab.com/dn13/oss/gitlab-templates/-/raw/main/slack-notify.yml
```
and then extend the job:

```yaml
docker:notify:
  extends: .docker:notify
  stage: release
  needs:
    - docker:release
```
You can also override some variables:
```yaml
docker:notify:
  extends: .docker:notify
  stage: release
  variables:
    IMAGE_NAME: <different image url>
    IMAGE_TAG: <different image tag>
    SLACK_CHANNEL: <#slack_channel>
  needs:
    - docker:release
```


For more information about the [project](https://gitlab.com/dn13/dn-tools/docker/slack-notify/-/blob/main/README.md).


## Restart K8s App
The `restart-k8s-app.yml` file provide jobs for restarting a kubernetes deployment and pulling the latest image.

You can use this remotely like this:
```yaml
include: https://gitlab.com/dn13/oss/gitlab-templates/-/raw/main/restart-k8s-app.yml
```
and then extend the job while overriding the variables:

```yaml
restart-k8s-app:
  extends: .restart-k8s-app
  variables:
    CONTEXT: <context>
    NAMESPACE: <namespace>
    RESTART_APP: <restart app name>
```

## Deploy Helm Charts
The `helm.yml` file provide jobs for deploying helm charts using Gitlab pages.

You can use this remotely like this:
```yaml
include: https://gitlab.com/dn13/oss/gitlab-templates/-/raw/main/helm.yml
```
and then add a varialble for the chart name like this:
```yaml
variables:
  HELM_CHART: test-chart
```



## License

Public domain, provided as-is without warranty.
