#################################### Include Needed pipeline templates #####################################
include:
  - remote: https://gitlab.com/dn13/oss/gitlab-templates/-/raw/main/teams-notify.yml
  - remote: https://gitlab.com/dn13/oss/gitlab-templates/-/raw/main/restart-k8s-app.yml



############################################ Gitflow Validation ############################################
protect:main:
  stage: validate-flow
  script: 'echo -e "You are not following gitflow!\n\nMain can only receive from develop, releases or hotfixes" && exit 1'
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "main" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME != "develop" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME != "uat" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^(hotfix|release)\//'
      when: always

protect:develop:
  stage: validate-flow
  script: 'echo -e "You are not following gitflow!\n\nDevelop can only receive features, releases or hotfixes" && exit 1'
  rules:
    - if: '$CI_MERGE_REQUEST_TARGET_BRANCH_NAME == "develop" && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME !~ /^(hotfix|feature|release)\//'
      when: always

prevent:variations:
  stage: validate-flow
  script: 'echo -e "You are not following gitflow!\n\nBranches must be either \`develop\`, \`main\` or start with \`feature/\`, \`release/\` or \`hotfix/\`.\n\nYour branch name was \`$CI_COMMIT_BRANCH\`" && exit 1'
  rules:
    - if: '$CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH != "develop" && $CI_COMMIT_BRANCH != "main" && $CI_COMMIT_BRANCH != "uat" && $CI_COMMIT_BRANCH !~ /^(hotfix|feature|release)\//'
      when: always



######################################## Staging release and deploy ########################################
release:staging:
  stage: release
  image:
    name: quay.io/skopeo/stable:latest
    entrypoint: [""]
  before_script:
    - skopeo login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - echo "Going to release \`$GITLAB_IMAGE_NAME:develop\`"
    - skopeo copy --multi-arch all docker://$GITLAB_IMAGE_NAME:$CI_COMMIT_SHA docker://$GITLAB_IMAGE_NAME:develop
  needs:
    - docker:build
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"

deploy:staging:
  stage: deploy
  extends: .restart-k8s-app
  variables:
    CONTEXT: VDFS
    NAMESPACE: staging
    RESTART_APP: $SERVICE_NAME
  needs:
    - release:staging
  rules:
    - if: $CI_COMMIT_BRANCH == "develop"
  environment:
    name: Staging
    url: https://${SERVICE_SUBDOMAIN}.staging.vdfs.d-n.be
    deployment_tier: staging



########################################## UAT release and deploy ##########################################
release:uat:
  stage: release
  image:
    name: quay.io/skopeo/stable:latest
    entrypoint: [""]
  before_script:
    - skopeo login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - echo "Going to release \`$GITLAB_IMAGE_NAME:latest\`"
    - skopeo copy docker://$GITLAB_IMAGE_NAME:$CI_COMMIT_SHA docker://$GITLAB_IMAGE_NAME:latest
  needs:
    - docker:build
  rules:
    - if: $CI_COMMIT_BRANCH == "uat"

deploy:uat:
  stage: deploy
  extends: .restart-k8s-app
  variables:
    CONTEXT: VDFS
    NAMESPACE: uat
    RESTART_APP: $SERVICE_NAME
  needs:
    - release:uat
  rules:
    - if: $CI_COMMIT_BRANCH == "uat"
  environment:
    name: UAT
    url: https://${SERVICE_SUBDOMAIN}.uat.vdfs.d-n.be
    deployment_tier: testing



#################################### Preflex/Preprod release and deploy ####################################
release:preflex:
  stage: release
  image:
    name: quay.io/skopeo/stable:latest
    entrypoint: [""]
  before_script:
    - skopeo login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - skopeo login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_HOST
  script:
    - echo "Going to release \`$OCI_IMAGE_NAME:latest\`"
    - skopeo copy docker://$GITLAB_IMAGE_NAME:$CI_COMMIT_SHA docker://$OCI_IMAGE_NAME:latest
  needs:
    - docker:build
  rules:
    - if: $CI_COMMIT_BRANCH == "main"

deploy:preflex:
  stage: deploy
  extends: .restart-k8s-app
  variables:
    CONTEXT: VDFS
    NAMESPACE: preflex
    RESTART_APP: $SERVICE_NAME
  needs:
    - release:preflex
  rules:
    - if: $CI_COMMIT_BRANCH == "main"
  environment:
    name: Preflex
    url: https://${SERVICE_SUBDOMAIN}.preflex.vdfleetsolutions.be
    deployment_tier: testing


####################################### Flex/Prod release and deploy #######################################
release:flex:
  stage: release
  image:
    name: quay.io/skopeo/stable:latest
    entrypoint: [""]
  before_script:
    - skopeo login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - skopeo login -u $REGISTRY_USER -p $REGISTRY_PASSWORD $REGISTRY_HOST
  script:
    - IMAGE_TAG=$(echo "$CI_COMMIT_TAG" | cut -d'/' -f2 -s)
    - echo "Going to release \`$OCI_IMAGE_NAME:$IMAGE_TAG\`"
    - skopeo copy docker://$GITLAB_IMAGE_NAME:$CI_COMMIT_SHA docker://$OCI_IMAGE_NAME:$IMAGE_TAG
  rules:
    # Only release if building from release or hotfix tag
    - if: '$CI_COMMIT_TAG =~ /^(release|hotfix)\//'

notify:flex:
  stage: deploy
  extends: .docker:notify
  variables:
    IMAGE_NAME: $OCI_IMAGE_NAME
  before_script:
    - IMAGE_TAG=$(echo "$CI_COMMIT_TAG" | cut -d'/' -f2 -s)
  needs:
    - release:flex
  rules:
    # Only release if building from release or hotfix tag
    - if: '$CI_COMMIT_TAG =~ /^(release|hotfix)\//'